﻿using Bootcamp.String.Library;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace BootCamp.String.Test
{
    public class StringFunctionsTest
    {
        [Fact]
        public void CountLetters()
        {
            string word = "14 letter word";
            int res = StringFunctions.GetLenght(word);
            Assert.Equal(res, word.Length);
        }

        [Fact]
        public void ToUpperCase()
        {
            string word = "14 letter word";
            string res = StringFunctions.ToUpperCase(word);
            Assert.Equal(res, word.ToUpper());
        }
    }
}
